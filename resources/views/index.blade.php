<!doctype html>
<html lang="en">
<head>
    <!-- important for compatibility charset -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>Service House - Serviços domésticos em Belém, Anandindeua, Marituba e Salinas</title>
    
    <meta name="author" content="Webful Creations">
    <meta name="keywords" content="">
    <meta name="description" content="">
    
    <!-- FavIcon for Website /-->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png">
    
    <!-- important for responsiveness remove to make your site non responsive. -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Theme Styles CSS File -->
    <link rel="stylesheet" type="text/css" href="style.css" media="all" />
</head>

<body>

	<div class="main-container">
    
    	<!-- Top Bar Starts-->
        <div class="topBar">
        	<div class="row">
            
            	<div class="large-3 medium-3 small-12 columns call-icon">
                    <p><i class="fa fa-phone" aria-hidden="true"></i>
                        (91) 3234-3949
                    </p>
                </div><!-- Call Icon /-->
                
                <div class="large-6 medium-5 small-12 columns border menu-centered bg-color">
                    <ul class="menu">
                        <li><a href="index.html#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="index.html#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="index.html#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <li><a href="index.html#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href="index.html#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div><!-- Social Icons /-->
                
                <div class="large-3 medium-4 small-12 columns right-topBar text-right">
                    <a href="index.html#"><i class="fa fa-envelope" aria-hidden="true"></i>contato@servicehouse.com.br</a>
                </div><!-- Right Top Bar /-->
            
            </div><!-- Row /-->
        </div>
        <!-- Top Bar /-->
        
        <!-- Header Starts-->
        <div class="header">
        	<div class="row">
            
            	<div class="large-3 medium-12 small-12 columns">
                	<div class="logo">
                    	<a href="index.html">
	                    	<img src="assets/images/logo-horizontal.png" alt="logo.site /">
                        </a>    
                    </div><!-- logo /-->
                </div><!-- Left columns /-->
                
                <div class="large-9 medium-12 small-12 columns nav-wrap">
                
                	<!-- navigation Code STarts here.. -->
                    <div class="top-bar">
						<div class="top-bar-title">
							<span data-responsive-toggle="responsive-menu" data-hide-for="large">
								<a data-toggle><span class="menu-icon dark float-left"></span></a>
							</span>
						</div>
                      
                        <nav id="responsive-menu">
                            <ul class="menu vertical large-horizontal float-right" data-responsive-menu="accordion large-dropdown">
                                <li class="single-sub parent-nav"><a href="index.html">Serviços</a>
                                    <ul class="child-nav menu vertical">
                                    	<li><a href="#">Babá</a></li>
                                        <li><a href="#">Cozinheira</a></li>
                                        <li><a href="#">Cuidador de Idoso</a></li>
                                        <li><a href="#">Faxineira</a></li>
                                        <li><a href="#">Lavadeira</a></li>
                                        <li><a href="#">Passadeira</a></li>
                                    </ul>
                                </li>

                                <li><a href="contact.html">Contato</a></li>
                            </ul>
                        </nav>
                    </div><!-- top-bar Ends -->
                    <!-- Navigation Code Ends here -->
                    
                    <div class="search-wrap float-right">
						<a href="index.html#" class="cart-icon-toggle" data-toggle="cart-dropdown">
							<i class="fa fa-shopping-basket"></i>
						</a><!-- Shopping Cart Button /-->
    					
						<a href="index.html#" class="search-icon-toggle" data-toggle="search-dropdown">
							<i class="fa fa-search"></i>
						</a><!-- Search Icon button /-->
                    </div><!-- search wrap ends -->

                
                </div><!-- right Columns /-->
                
            </div><!--row /-->
        </div>
        <!-- Header /-->

        <div class="slide transparent-background">
            <div class="row">

                <div class="medium-12 small-12 columns">
                    <div class="banner-text">
                        <h2>Bem vindo à <span>Service House</span> PRAZER DE CUIDAR DE QUEM VOCÊ AMA</h2>
                        <h3><i class="fa fa-check" aria-hidden="true"></i>Profissionais altamente capacitadas
                            <br /><i class="fa fa-check" aria-hidden="true"></i>Confiança e profissionalismo.</h3>
                    </div><!-- Banner Text /-->
                    <div class="banner-image">
                        <img src="assets/images/help/banner-img2.png" alt="banner-img" />
                    </div><!--Banner Image /-->
                </div><!-- columns /-->

            </div><!-- Row /-->
        </div><!-- Slide /-->
        <!-- Main Banner /-->


        <div class="services module grey-bg">

            <div class="section-title small-module">
                <h2><span>Nossos</span> Serviços</h2>
                <p>Conheça nossos serviços</p>
            </div><!-- Section Title /-->

            <div class="row">

                <?php $servicos = ['Babá', 'Cozinheira', 'Cuidadora de Idoso', 'Faxineira', 'Lavadeira', 'Passadeira']; ?>

                @foreach ($servicos as $s)
                <div class="small-12 medium-4 columns">
                    <div class="service">
                        <div class="service-icon">
                            <a href="#">
                                <i class="fa fa-child" aria-hidden="true"></i>
                            </a>
                        </div><!-- Service Icon /-->
                        <div class="service-text">
                            <a href="#"><h2>{{ $s }}</h2></a>
                            <p>Este serviço é realizado por nossos profissionais super capacitados</p>
                        </div><!-- Service Text -->
                    </div><!-- Service /-->
                </div><!-- Columns /-->
                @endforeach


            </div><!-- row /-->
        </div>
        <!-- Services /-->


        <div class="service-banner">
        	<div class="row relative-position">
            
                <img src="assets/images/help/babysitter.png" alt="Service Provider Image"/>
                
                <div class="medium-9 small-12 columns medium-offset-3 services-box">
                    <h2><i class="fa fa-life-ring" aria-hidden="true"></i>Estamos sempre prontos pra lhe atender!</h2>
                    <p>Estamos muito felizes em anunciar que temos alguns ótimos serviços para oferecer em sua área ao redor do ano! Como a nossa missão é fornecer os melhores serviços na cidade, é por isso que somos formidáveis.</p>
                	<div class="call-box"><i class="fa fa-phone"></i> (91) 9999-9999 &nbsp;&nbsp;&nbsp;<i class="fa fa-envelope"></i> contato@servicehousebelem.com.br</div>
                </div><!-- Columns /-->
            	
            </div><!-- Row /-->
        </div>
        <!-- Need Service /-->
        

     	
        <!-- Onde atendemos -->
        <div class="why-choose-us module">
            <div class="why-choose-us-border">
            	
            	<div class="section-title small-module">
            		<h2><span>Cobertura </span>dos serviços</h2>
                	{{--<p>We are offer best prices for our customer</p>--}}
            	</div><!-- Section Title /-->
            	
            	<div class="row">
                	<div class="small-12 medium-6 columns">


                	
                    	<div class="why-us-wrapper">
                            <h1>-</h1>
                    		<h2>Belém</h2>
                            <p>Atendemos na região metropolitana de Belém</p>

                    	</div><!-- Why Us Wrapper /-->
                	
                    	<div class="why-us-wrapper">
                            <h1>-</h1>
                    		<h2>Ananindeua</h2>
                            <p>Atendemos na região metropolitana de Belém</p>
                    	</div><!-- Why Us Wrapper /-->
                    
                    	<div class="why-us-wrapper">
                    		<h1>-</h1>
                    		<h2>Marituba</h2>
                            <p>Atendemos na região metropolitana de Belém</p>
                    	</div><!-- Why Us Wrapper /-->

                        <div class="why-us-wrapper">
                            <h1>-</h1>
                            <h2>Salinas</h2>
                            <p>Atendemos em feriados e finais de semana.</p>
                        </div><!-- Why Us Wrapper /-->
                    
                	</div><!-- Columns /-->
            
            	</div><!-- Row /-->
            </div><!--Why Choose Us Border /-->
        </div>    
        <!-- Why Choose Us /-->
        
      <div class="testimonials dark-bg">
        	
            <div class="section-title">
            	<h2>Depoimentos de clientes</h2>
				<p>Veja como nós deixamos nossos clientes felizes!</p>
            </div><!-- Section Title /-->
        	
            <div class="testimonials-box">
      			
                <div class="slide">
            	    <div class="row">

                        <div class="medium-6 small-12 columns">
                            <div class="testimonials-section">
                                <div class="title">
                                    <img src="assets/images/help/testimonials/test1.jpg" alt=""/>
                                    <span class="name">Fernanda Maria</span>
                                    <span class="designation">Serviço de Babá</span>
                                    <p><span>,,</span><br> O serviço foi excelente! Profissionais super atenciosos, carinhosos e dedicados! Recomendo o serviço! </p>
                                </div><!-- Title /-->
                            </div><!-- Testimonials Section /-->
                        </div><!-- Columns /-->

                        <div class="medium-6 small-12 columns">
                            <div class="testimonials-section">
                                <div class="title">
                                    <img src="assets/images/help/testimonials/test2.jpg" alt=""/>
                                    <span class="name">Jonas Filho</span>
                                    <span class="designation">Cuidadora de idoso</span>
                                    <p><span>,,</span><br> O serviço foi excelente! Profissionais super atenciosos, carinhosos e dedicados! Recomendo o serviço! </p>
                                </div><!-- Title /-->
                            </div><!-- Testimonials Section /-->
                        </div><!-- Columns /-->

                    </div><!-- Row /-->
            	</div><!-- Slide /-->


                <div class="slide">
                    <div class="row">

                        <div class="medium-6 small-12 columns">
                            <div class="testimonials-section">
                                <div class="title">
                                    <img src="assets/images/help/testimonials/test1.jpg" alt=""/>
                                    <span class="name">Fernanda Maria</span>
                                    <span class="designation">Serviço de Babá</span>
                                    <p><span>,,</span><br> O serviço foi excelente! Profissionais super atenciosos, carinhosos e dedicados! Recomendo o serviço! </p>
                                </div><!-- Title /-->
                            </div><!-- Testimonials Section /-->
                        </div><!-- Columns /-->

                        <div class="medium-6 small-12 columns">
                            <div class="testimonials-section">
                                <div class="title">
                                    <img src="assets/images/help/testimonials/test2.jpg" alt=""/>
                                    <span class="name">Jonas Filho</span>
                                    <span class="designation">Cuidadora de idoso</span>
                                    <p><span>,,</span><br> O serviço foi excelente! Profissionais super atenciosos, carinhosos e dedicados! Recomendo o serviço! </p>
                                </div><!-- Title /-->
                            </div><!-- Testimonials Section /-->
                        </div><!-- Columns /-->

                    </div><!-- Row /-->
                </div><!-- Slide /-->
                

                

                
            </div><!-- Testimonials Box /-->
        </div>
        <!-- Testimonials /-->
        

        
        <!-- Call To Action Box -->
		<div class="call-to-action">
			<div class="row">
				
				<div class="medium-12 small-12 columns">
					<div class="float-left">
						<h2>Nós oferecemos <span>Serviços domésticos profissionais</span></h2>
						<p>Trabalhamos todos os dias, tudo para atender sua necessidade!</p>
					</div><!-- Float Left /-->
					<div class="float-right call-us">
						<div class="float-left callus-icon">
							<i class="fa fa-volume-control-phone"></i>
						</div>
						<div class="float-right text-center">
							<h2>Ligue agora!</h2>
							<p>(91) 9999-9999</p>
						</div>
					</div><!-- Float Right /-->
					<div class="clearfix"></div>	
				</div><!-- Column Ends /-->
				
			</div><!-- Row /-->
		</div>
        <!-- Call To Action Box Ends /-->
        
        <!-- Footer -->
        <div class="footer">
        	
            <div class="footer-top">
            	<div class="row">
        	
            		<div class="medium-3 small-12 columns">
                        <div class="footer-box">
                            <img src="assets/images/logo-horizontal.png" alt=""/>
                            <ul>
                                <li><i class="fa fa-map-marker"></i><a href="https://www.webfulcreations.com/envato/plumber_template/contact-us.html">Belém, PA - BRASIL</a></li>
                                <li><i class="fa fa-phone"></i><a href="tel:1231231234">(91) 9998-9999</a></li>
                                <li><i class="fa fa-envelope-o"></i><a href="index.html#">contato@servicehousebelem.com.br</a></li>
                            </ul>
                        </div><!-- Footer Box /-->
            		</div><!-- Columns /-->
                
            		<div class="medium-3 small-12 columns">
                        <div class="footer-box border-btm">
                            <h2>NOSSOS SERVIÇOS</h2>
                            <ul>
                                <li><a href="#">Babá</a></li>
                                <li><a href="#">Cozinheira</a></li>
                                <li><a href="#">Cuidador de Idoso</a></li>
                                <li><a href="#">Faxineira</a></li>
                                <li><a href="#">Lavadeira</a></li>
                                <li><a href="#">Passadeira</a></li>
                            </ul>
                        </div><!-- Footer Box /-->
                	</div><!-- Columns /-->
                
                	<div class="medium-3 small-12 columns end">
                        <div class="footer-box border-btm">
                            <h2>Fale Conosco</h2>
                            <ul>
                                <li><a href="#">Contato</a></li>
                            </ul>
                        </div><!-- Footer Box /-->
                	</div><!-- Columns /-->
                
        		</div><!-- Row /-->
        	</div><!-- Footer Top /-->
            
            <div class="footer-bottom">
            	<div class="row">
                
                    <div class="medium-6 small-12 columns">
                        <div class="copyrightinfo">2017 &copy; <a href="http://servicehousebelem.com.br">Service House</a> Todos os Direitos Reservados</div>
                    </div><!-- Left Column Ends /-->
                    
                    <div class="medium-6 small-12 columns">
                        <div class="pull-right">
                            <ul class="menu">
                                <li><a href="index.html#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="index.html#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="index.html#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="index.html#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                <li><a href="index.html#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div><!-- Right Column Ends /-->
                
                </div><!-- Row /-->
            </div><!-- Footer Bottom /-->
            
        </div>
        <!-- Footer /-->
        
    </div><!-- Main Container /-->
    
    <!-- Move to Top Icon 
    	 Remove to Not Display /-->
    <a href="index.html#" id="top" title="Go to Top">
    	<i class="fa fa-long-arrow-up"></i>
    </a>
    
    <!-- Page Preloader
    	 Delete to Remove Preloader /-->
    <div class="preloader">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
	</div><!-- Preloader /-->
        
    <!-- Including Jquery so All js Can run -->
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    
    <!-- Including Foundation JS so Foundation function can work. -->
    <script type="text/javascript" src="assets/js/foundation.min.js"></script>
    
	<!-- Carousel JS -->
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
   
	<!-- Jquery Lightbox jS -->
    <script type="text/javascript" src="assets/js/lightbox.min.js"></script>
    
    <!-- Webful JS -->
    <script src="assets/js/template.js"></script> 
</body>
</html>    